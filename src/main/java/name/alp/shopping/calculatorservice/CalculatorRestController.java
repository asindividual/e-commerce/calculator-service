package name.alp.shopping.calculatorservice;

import io.swagger.v3.oas.annotations.Operation;
import name.alp.shopping.calculatorservice.client.shoppingcart.ShoppingCart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class CalculatorRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorRestController.class);
    private final CalculatorService calculatorService;

    public CalculatorRestController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }


    @Operation(summary = "Applies the discounts to the cart according to the campigns",
            description = "")
    @PostMapping("/applyDiscounts")
    public ShoppingCart applyDiscountsToCart(@RequestBody ShoppingCart shoppingCart) {
        LOGGER.info("applyDiscountsToCart: {}", shoppingCart);
        return calculatorService.applyDiscountsToCart(shoppingCart);
    }

    @Operation(summary = "Applies the coupon if it is applicable to the cart",
            description = "")
    @PostMapping("/applyCoupon/{couponId}")
    public ShoppingCart applyCouponToCart(@RequestBody ShoppingCart shoppingCart,@PathVariable("couponId") String couponId) {
        LOGGER.info("applyCouponToCart: {}", shoppingCart);
        return calculatorService.applyCoupons(shoppingCart,couponId);
    }


    @Operation(summary = "Calculates delivery cost",
            description = "")
    @PostMapping("calculateLogisticsCost")
    public ShoppingCart calculateLogisticsCost(@RequestBody ShoppingCart shoppingCart) {
        LOGGER.info("calculateLogisticsCost: {}", shoppingCart);
        return calculatorService.calculateLogisticsCost(shoppingCart);
    }

}
