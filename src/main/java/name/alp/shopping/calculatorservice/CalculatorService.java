package name.alp.shopping.calculatorservice;

import name.alp.shopping.calculatorservice.client.campaign.Campaign;
import name.alp.shopping.calculatorservice.client.campaign.CampaignClient;
import name.alp.shopping.calculatorservice.client.category.Category;
import name.alp.shopping.calculatorservice.client.category.CategoryClient;
import name.alp.shopping.calculatorservice.client.coupon.Coupon;
import name.alp.shopping.calculatorservice.client.coupon.CouponClient;
import name.alp.shopping.calculatorservice.client.logistics.Logistics;
import name.alp.shopping.calculatorservice.client.logistics.LogisticsClient;
import name.alp.shopping.calculatorservice.client.products.ProductsClient;
import name.alp.shopping.calculatorservice.client.shoppingcart.DiscountAppliedProductInstance;
import name.alp.shopping.calculatorservice.client.shoppingcart.ProductInstance;
import name.alp.shopping.calculatorservice.client.shoppingcart.ShoppingCart;
import name.alp.shopping.calculatorservice.client.shoppingcart.ShoppingCartClient;
import name.alp.shopping.calculatorservice.exceptions.CouponAlreadyAppliedToChartException;
import name.alp.shopping.calculatorservice.exceptions.CouponCannotBeAppliedToChartException;
import name.alp.shopping.calculatorservice.exceptions.InvalidCouponException;
import name.alp.shopping.calculatorservice.exceptions.LogisticCompanyIsNullException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class CalculatorService {

    private final CategoryClient categoryClient;
    private final CampaignClient campaignClient;
    private final CouponClient couponClient;
    private final LogisticsClient logisticsClient;
    private final ProductsClient productsClient;
    private final ShoppingCartClient shoppingCartClient;


    public CalculatorService(CategoryClient categoryClient,
                             CampaignClient campaignClient,
                             CouponClient couponClient,
                             LogisticsClient logisticsClient,
                             ProductsClient productsClient,
                             ShoppingCartClient shoppingCartClient) {

        this.categoryClient = categoryClient;
        this.campaignClient = campaignClient;
        this.couponClient = couponClient;
        this.logisticsClient = logisticsClient;
        this.productsClient = productsClient;
        this.shoppingCartClient = shoppingCartClient;
    }

    public ShoppingCart applyDiscountsToCart(ShoppingCart shoppingCart) {

        shoppingCart.setAppliedCampaignsDiscounts(new ArrayList<>());
        double totalDiscounts=0;
        double totalCost=0;
        for(ProductInstance productInstance:shoppingCart.getProductInstances()){
            totalCost+=productInstance.getProduct().getPrice() * productInstance.getAmount();
            List<Campaign> campaigns=campaignClient.findApplicableCampaignsByCategoryId(productInstance.getProduct().getCategory(),productInstance.getAmount());
            DiscountAppliedProductInstance discountAppliedProductInstance=new DiscountAppliedProductInstance();
            for(Campaign campaign:campaigns) {
                double discount=0;
                switch (campaign.getDiscountType()) {
                    case Rate:
                        discount=productInstance.getProduct().getPrice() * productInstance.getAmount() * (( campaign.getDiscount()) / 100);
                        break;
                    case Amount:
                        discount= campaign.getDiscount();
                        break;
                }
                totalDiscounts+= discount;
                discountAppliedProductInstance.getAppliedDiscounts().add(new DiscountAppliedProductInstance.AppliedDiscount(discount,campaign,productInstance));
            }
            shoppingCart.getAppliedCampaignsDiscounts().add(discountAppliedProductInstance);
        }
        shoppingCart.setCalculatedCostAfterCampaignsDiscounts(totalCost-totalDiscounts);

        return shoppingCart;
    }



    public ShoppingCart applyCoupons(ShoppingCart shoppingCart,String couponId) {
        if(shoppingCart.getAppliedCoupons().contains(couponId)){
            throw new CouponAlreadyAppliedToChartException(couponId);
        }

        Coupon newcoupon=couponClient.findById(couponId);
        if(newcoupon==null){
            throw new InvalidCouponException(couponId);
        }
        if( (shoppingCart.getCalculatedCostAfterCouponDiscounts()!=0&& shoppingCart.getCalculatedCostAfterCouponDiscounts() < newcoupon.getMinimumAmountToUse()) ){
            throw new CouponCannotBeAppliedToChartException(shoppingCart.getCalculatedCostAfterCouponDiscounts(),newcoupon.getMinimumAmountToUse());
        }
        if(  (shoppingCart.getCalculatedCostAfterCouponDiscounts()!=0&& shoppingCart.getCalculatedCostAfterCampaignsDiscounts() < newcoupon.getMinimumAmountToUse()) ){
            throw new CouponCannotBeAppliedToChartException(shoppingCart.getCalculatedCostAfterCampaignsDiscounts(),newcoupon.getMinimumAmountToUse());
        }

        shoppingCart.getAppliedCoupons().add(newcoupon);

        double totalDiscount=0;
        for(Coupon coupon:shoppingCart.getAppliedCoupons()){
            double discount=0;
            switch (coupon.getDiscountType()) {
                case Rate:
                    discount=shoppingCart.getCalculatedCostAfterCampaignsDiscounts() * (( coupon.getDiscount()) / 100);
                    break;
                case Amount:
                    discount= coupon.getDiscount();
                    break;
            }
            totalDiscount+=discount;

        }
        shoppingCart.setCalculatedCostAfterCouponDiscounts(shoppingCart.getCalculatedCostAfterCouponDiscounts()-totalDiscount);
        return shoppingCart;

    }

    public ShoppingCart calculateLogisticsCost(ShoppingCart shoppingCart) {
        if(shoppingCart.getSelectedLogisticsCompany()==null){
            throw new LogisticCompanyIsNullException();
        }
        Logistics selectedLogisticsCompany = logisticsClient.findById(shoppingCart.getSelectedLogisticsCompany());
        List<Category> categoriesList=new ArrayList<>();
        int numberOfProducts=0;
        for(ProductInstance productInstance:shoppingCart.getProductInstances()){
            numberOfProducts+=productInstance.getAmount();
            categoriesList.add(productInstance.getProduct().getCategory());
        }
        categoriesList.stream().distinct();
        int numberOfDeliveries=    categoriesList.size();
        double logisticsCost= (numberOfDeliveries*selectedLogisticsCompany.getCostPerDelivery())+(numberOfProducts*selectedLogisticsCompany.getCostPerProduct())+selectedLogisticsCompany.getFixedCost();

        shoppingCart.setCalculatedCostWithLogisticsCost(logisticsCost);

        return shoppingCart;
    }

}
