package name.alp.shopping.calculatorservice.exceptions;

public class InvalidCouponException extends RuntimeException {
    public InvalidCouponException(String couponId) {
        super("This Coupon is not valid id:" + couponId);
    }
}
