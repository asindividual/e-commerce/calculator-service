package name.alp.shopping.calculatorservice.exceptions;

public class CategoryNotFoundException extends RuntimeException {
    public CategoryNotFoundException(String categoryId) {
        super("Category Not Found path:" + categoryId);
    }
}
