package name.alp.shopping.calculatorservice.exceptions;

public class LogisticCompanyIsNullException extends RuntimeException {
    public LogisticCompanyIsNullException() {
        super("You must select the logistics company to calculate Logistics cost");
    }
}
