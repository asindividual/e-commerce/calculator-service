package name.alp.shopping.calculatorservice.exceptions;

public class CouponAlreadyAppliedToChartException extends RuntimeException {
    public CouponAlreadyAppliedToChartException(String couponId) {
        super("This Coupon already included into applied to the chart id:" + couponId);
    }
}
