package name.alp.shopping.calculatorservice.exceptions;

public class CouponCannotBeAppliedToChartException extends RuntimeException {
    public CouponCannotBeAppliedToChartException(Double calculatedCost, Integer minimumAmountToUse) {
        super("This Coupon can not be applied to the chart chart amount:" + calculatedCost +"Coupon minimum amount:"+minimumAmountToUse);
    }
}
