package name.alp.shopping.calculatorservice.client.shoppingcart;

import name.alp.shopping.calculatorservice.client.coupon.Coupon;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class ShoppingCart {
    private String id;
    private List<ProductInstance> productInstances;
    private List<DiscountAppliedProductInstance> appliedCampaignsDiscounts;
    private List<Coupon> appliedCoupons;
    private Double calculatedCostAfterCampaignsDiscounts;
    private Double calculatedCostAfterCouponDiscounts;
    private Double calculatedCostWithLogisticsCost;
    private String selectedLogisticsCompany;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dateCreated;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date dateUpdated;
    private CartStatus currentCartStatus;


    public ShoppingCart() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ProductInstance> getProductInstances() {
        return productInstances;
    }

    public void setProductInstances(List<ProductInstance> productInstances) {
        this.productInstances = productInstances;
    }

    public List<DiscountAppliedProductInstance> getAppliedCampaignsDiscounts() {
        return appliedCampaignsDiscounts;
    }

    public void setAppliedCampaignsDiscounts(List<DiscountAppliedProductInstance> appliedCampaignsDiscounts) {
        this.appliedCampaignsDiscounts = appliedCampaignsDiscounts;
    }

    public List<Coupon> getAppliedCoupons() {
        return appliedCoupons;
    }

    public void setAppliedCoupons(List<Coupon> appliedCoupons) {
        this.appliedCoupons = appliedCoupons;
    }

    public String getSelectedLogisticsCompany() {
        return selectedLogisticsCompany;
    }

    public void setSelectedLogisticsCompany(String selectedLogisticsCompany) {
        this.selectedLogisticsCompany = selectedLogisticsCompany;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public CartStatus getCurrentCartStatus() {
        return currentCartStatus;
    }

    public void setCurrentCartStatus(CartStatus currentCartStatus) {
        this.currentCartStatus = currentCartStatus;
    }

    public Double getCalculatedCostAfterCampaignsDiscounts() {
        return calculatedCostAfterCampaignsDiscounts;
    }

    public void setCalculatedCostAfterCampaignsDiscounts(Double calculatedCostAfterCampaignsDiscounts) {
        this.calculatedCostAfterCampaignsDiscounts = calculatedCostAfterCampaignsDiscounts;
    }

    public Double getCalculatedCostAfterCouponDiscounts() {
        return calculatedCostAfterCouponDiscounts;
    }

    public void setCalculatedCostAfterCouponDiscounts(Double calculatedCostAfterCouponDiscounts) {
        this.calculatedCostAfterCouponDiscounts = calculatedCostAfterCouponDiscounts;
    }

    public Double getCalculatedCostWithLogisticsCost() {
        return calculatedCostWithLogisticsCost;
    }

    public void setCalculatedCostWithLogisticsCost(Double calculatedCostWithLogisticsCost) {
        this.calculatedCostWithLogisticsCost = calculatedCostWithLogisticsCost;
    }
}
