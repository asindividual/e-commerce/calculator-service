package name.alp.shopping.calculatorservice.client.logistics;

import name.alp.shopping.calculatorservice.client.BaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

@Service
public class LogisticsClient extends BaseClient<Logistics> {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogisticsClient.class);

    public LogisticsClient(DiscoveryClient client, @Value("${feign.services.logistics-service.baseUrl}") String logisticsServiceBaseUrl) {
        super(client, "logistics-service", logisticsServiceBaseUrl);
    }
}
