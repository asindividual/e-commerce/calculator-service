package name.alp.shopping.calculatorservice.client.campaign;

import name.alp.shopping.calculatorservice.client.BaseClient;
import name.alp.shopping.calculatorservice.client.category.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class CampaignClient extends BaseClient<Campaign> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CampaignClient.class);

    public CampaignClient(DiscoveryClient client, @Value("${feign.services.campaign-service.baseUrl}") String campaignServiceBaseUrl) {
        super(client, "campaign-service", campaignServiceBaseUrl);
    }



    public List<Campaign> findApplicableCampaignsByCategoryId(Category category,Integer totalamount) {

        WebClient webClient = getWebClient(getService());
        AtomicReference<List<Campaign>> atomicCategory = new AtomicReference<>();

        String uri = getBaseUrl(getService()) + "category/"+category.getId()+"/totalamount/"+totalamount.intValue();
        Mono<List<Campaign>> serviceCall =  webClient.get()
                .uri(uri)
                .retrieve()
                .bodyToFlux(Campaign.class)
                .collectList().doOnSuccess(campaigns -> atomicCategory.set(campaigns));

        Disposable subscribe = serviceCall.subscribe();
        List<Campaign> result = block(subscribe, atomicCategory);

        return result;
    }

}
