package name.alp.shopping.calculatorservice.client.shoppingcart;


import name.alp.shopping.calculatorservice.client.campaign.Campaign;

import java.util.ArrayList;
import java.util.List;

public class DiscountAppliedProductInstance extends ProductInstance{

    List<AppliedDiscount> appliedDiscounts=new ArrayList<>();


    public DiscountAppliedProductInstance() {

    }

    public List<AppliedDiscount> getAppliedDiscounts() {
        return appliedDiscounts;
    }

    public void setAppliedDiscounts(List<AppliedDiscount> appliedDiscounts) {
        this.appliedDiscounts = appliedDiscounts;
    }

    public static class AppliedDiscount{
        private Double appliedCampaignDiscount;
        private Campaign campaign;
        private ProductInstance productInstance;
        public AppliedDiscount() {
        }

        public AppliedDiscount(Double appliedCampaignDiscount, Campaign campaign, ProductInstance productInstance) {
            this.appliedCampaignDiscount = appliedCampaignDiscount;
            this.campaign = campaign;
            this.productInstance = productInstance;
        }

        public Double getAppliedCampaignDiscount() {
            return appliedCampaignDiscount;
        }

        public void setAppliedCampaignDiscount(Double appliedCampaignDiscount) {
            this.appliedCampaignDiscount = appliedCampaignDiscount;
        }

        public Campaign getCampaign() {
            return campaign;
        }

        public void setCampaign(Campaign campaign) {
            this.campaign = campaign;
        }

        public ProductInstance getProductInstance() {
            return productInstance;
        }

        public void setProductInstance(ProductInstance productInstance) {
            this.productInstance = productInstance;
        }
    }
}
