package name.alp.shopping.calculatorservice.client.coupon;

import name.alp.shopping.calculatorservice.client.BaseClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

@Service
public class CouponClient extends BaseClient<Coupon> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CouponClient.class);

    public CouponClient(DiscoveryClient client, @Value("${feign.services.coupon-service.baseUrl}") String couponServiceBaseUrl) {
        super(client, "coupon-service", couponServiceBaseUrl);
    }
}
