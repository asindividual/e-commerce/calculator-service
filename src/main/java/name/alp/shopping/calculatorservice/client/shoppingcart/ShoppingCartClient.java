package name.alp.shopping.calculatorservice.client.shoppingcart;

import name.alp.shopping.calculatorservice.client.BaseClient;
import name.alp.shopping.calculatorservice.client.products.ProductsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartClient extends BaseClient<ShoppingCart> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductsClient.class);

    public ShoppingCartClient(DiscoveryClient client, @Value("${feign.services.shopping-cart-service.baseUrl}") String shoppingCartServiceBaseUrl) {
        super(client, "shopping-cart-service", shoppingCartServiceBaseUrl);
    }
}
