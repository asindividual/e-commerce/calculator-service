package name.alp.shopping.calculatorservice.client.logistics;


import java.util.List;

public class Logistics {
    private String id;
    private String companyName;
    private List<String> fromCities;
    private List<String> toCities;
    private Double maxDeci;
    private Double logisticsCost;

    private Double costPerDelivery;
    private Double costPerProduct;
    private Double fixedCost;

    public Logistics() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<String> getFromCities() {
        return fromCities;
    }

    public void setFromCities(List<String> fromCities) {
        this.fromCities = fromCities;
    }

    public List<String> getToCities() {
        return toCities;
    }

    public void setToCities(List<String> toCities) {
        this.toCities = toCities;
    }

    public Double getMaxDeci() {
        return maxDeci;
    }

    public void setMaxDeci(Double maxDeci) {
        this.maxDeci = maxDeci;
    }

    public Double getLogisticsCost() {
        return logisticsCost;
    }

    public void setLogisticsCost(Double logisticsCost) {
        this.logisticsCost = logisticsCost;
    }

    public Double getCostPerDelivery() {
        return costPerDelivery;
    }

    public void setCostPerDelivery(Double costPerDelivery) {
        this.costPerDelivery = costPerDelivery;
    }

    public Double getCostPerProduct() {
        return costPerProduct;
    }

    public void setCostPerProduct(Double costPerProduct) {
        this.costPerProduct = costPerProduct;
    }

    public Double getFixedCost() {
        return fixedCost;
    }

    public void setFixedCost(Double fixedCost) {
        this.fixedCost = fixedCost;
    }
}
